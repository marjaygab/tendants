-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2018 at 03:15 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aces`
--

-- --------------------------------------------------------

--
-- Table structure for table `adminaccounts`
--

CREATE TABLE `adminaccounts` (
  `id` int(11) NOT NULL,
  `Username` varchar(500) NOT NULL,
  `Password` varchar(500) NOT NULL,
  `First_Name` varchar(500) NOT NULL,
  `Last_Name` varchar(500) NOT NULL,
  `Position` varchar(500) NOT NULL,
  `Middle_Initial` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adminaccounts`
--

INSERT INTO `adminaccounts` (`id`, `Username`, `Password`, `First_Name`, `Last_Name`, `Position`, `Middle_Initial`) VALUES
(1, 'marjaygab', 'tapaymarj', 'Marjay Gabriel', 'Tapay', 'Public Relations Officer', 'P.');

-- --------------------------------------------------------

--
-- Table structure for table `events_list`
--

CREATE TABLE `events_list` (
  `EventName` varchar(500) NOT NULL,
  `Date` date NOT NULL,
  `Venue` varchar(500) NOT NULL,
  `TimeStart` time NOT NULL,
  `TimeEnd` time NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events_list`
--

INSERT INTO `events_list` (`EventName`, `Date`, `Venue`, `TimeStart`, `TimeEnd`, `id`) VALUES
('PCB Workshop', '2017-09-27', 'MB 208', '13:00:00', '17:00:00', 1),
('CpEquip : Cyber Security', '2018-04-06', 'CB Function Hall', '11:00:00', '17:00:00', 3);

-- --------------------------------------------------------

--
-- Table structure for table `event_1`
--

CREATE TABLE `event_1` (
  `id` int(11) UNSIGNED NOT NULL,
  `Student_Number` varchar(300) NOT NULL,
  `First_Name` varchar(500) NOT NULL,
  `Middle_Initial` varchar(50) NOT NULL,
  `Last_Name` varchar(300) NOT NULL,
  `Year` varchar(300) NOT NULL,
  `Address` varchar(300) DEFAULT NULL,
  `Gender` varchar(300) DEFAULT NULL,
  `Age` varchar(300) DEFAULT NULL,
  `Contact_Number` varchar(300) DEFAULT NULL,
  `Time_of_Arrival` time NOT NULL,
  `Section` varchar(300) NOT NULL,
  `EventID` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event_1`
--

INSERT INTO `event_1` (`id`, `Student_Number`, `First_Name`, `Middle_Initial`, `Last_Name`, `Year`, `Address`, `Gender`, `Age`, `Contact_Number`, `Time_of_Arrival`, `Section`, `EventID`) VALUES
(1, '2014141511', 'Marjay Gabriel', 'P.`', 'Tapay', '4th', NULL, NULL, NULL, NULL, '12:59:36', 'O4B', 0),
(2, '2015169721', 'Michael Kenji ', 'S.', 'Galicinao', '3rd', NULL, NULL, NULL, NULL, '13:10:43', 'O3A', 0),
(3, '2015179751', 'Mark Anton', 'V', 'Villegas', '3rd', NULL, NULL, NULL, NULL, '13:11:11', 'O3A', 0),
(5, '2015178651', 'Janrald Rae', 'M.', 'Miranda', '3rd', NULL, NULL, NULL, NULL, '13:11:59', 'O3A', 0),
(6, '2015167921', 'Louis John', 'A', 'Doblon', '3rd', NULL, NULL, NULL, NULL, '13:12:20', 'O3A', 0),
(7, '2015168751', 'Faye', 'M', 'Sumudlayon', '3rd year', NULL, NULL, NULL, NULL, '13:12:54', 'o3b', 0),
(8, '2015169091', 'Agnes May ', 'B.', 'Kalaw', '3rd', NULL, NULL, NULL, NULL, '13:13:28', 'O3A', 0),
(9, '2015181141', 'Kazuaki', 'S.', 'Abe', '3', NULL, NULL, NULL, NULL, '13:14:40', 'O3A', 0),
(10, '2015168441', 'Leonardo', 'B.', 'Calingasan II', '3rd', NULL, NULL, NULL, NULL, '13:15:22', 'O3A', 0),
(11, '2015175731', 'John Lhinel', 'A.', 'Pablo', '3rd', NULL, NULL, NULL, NULL, '13:16:09', 'O3A', 0),
(12, '2015178621', 'Joshua Byron', 'P.', 'Catibog', '3rd', NULL, NULL, NULL, NULL, '13:16:33', 'O3A', 0),
(13, '2015179991', 'Renzel', 'M.', 'Curugan', '3rd', NULL, NULL, NULL, NULL, '13:17:12', 'O3A', 0),
(14, '2015184361', 'Mishel Kate ', 'M', 'Aguilar', '3rd', NULL, NULL, NULL, NULL, '13:17:37', 'O3A', 0),
(15, '2015169521', 'Russel ', 'M.', 'Marasigan', '3rd', NULL, NULL, NULL, NULL, '13:18:13', 'O3A', 0),
(16, '2015176591', 'Joshua Ehrick', 'A.', 'Mitra', '3rd', NULL, NULL, NULL, NULL, '13:18:55', 'O3A', 0),
(17, '2015168801', 'Benzen Leonard', 'P.', 'Montalbod', '3rd', NULL, NULL, NULL, NULL, '13:20:00', 'O3A', 0),
(18, '2015172551', 'Keith Harvey', 'D.', 'Ayop', '3rd', NULL, NULL, NULL, NULL, '13:20:33', 'O3A', 0),
(19, '2015171761', 'Dave', 'L.', 'Valencia', '3rd', NULL, NULL, NULL, NULL, '13:21:10', 'O3A', 0),
(20, '2014146881', 'Vedder Moses', 'M', 'Sanohan', '4th', NULL, NULL, NULL, NULL, '13:21:34', 'O3A', 0),
(21, '2015175631', 'Russell Floyd', 'G.', 'Del Monte', '3rd', NULL, NULL, NULL, NULL, '13:22:02', 'O3B', 0),
(22, '2015177061', 'Don Den Cole', 'O', 'Isaga', '3rd', NULL, NULL, NULL, NULL, '13:22:24', 'O3A', 0),
(23, '2014140941', 'Kieth Arneil', 'M.', 'Rodelas', '4th', NULL, NULL, NULL, NULL, '13:23:10', 'O3B', 0),
(24, '2014156491', 'John Michael', 'D.', 'Guantero', '4th', NULL, NULL, NULL, NULL, '13:23:42', 'O3B', 0),
(25, '2015184301', 'Maharanee Ariane ', 'P', 'Borlaza', '3rd', NULL, NULL, NULL, NULL, '13:25:15', 'O3B', 0),
(26, '2015175451', 'Christian John ', 'C.', 'Laqui', '3rd Year', NULL, NULL, NULL, NULL, '15:27:21', '03A', 0),
(27, '2015167811', 'renz nicole', 'H.', 'deleon', '3rd', NULL, NULL, NULL, NULL, '15:27:53', 'O3B', 0),
(28, '2015168741', 'Vincenzo Carlos', 'V.', 'Villaluz', '3rd', NULL, NULL, NULL, NULL, '15:28:47', 'O3B', 0),
(29, '2015177111', 'Shyla Marie', 'D.', 'Escal', '3rd', NULL, NULL, NULL, NULL, '15:29:18', 'O3B', 0),
(30, '2015177291', 'Diana Rose Apple', 'M.', 'Roncesvalles', '3rd', NULL, NULL, NULL, NULL, '15:29:54', 'O3B', 0),
(31, '2015177281', 'Kim Alexis', 'S.', 'Miranda', '3rd', NULL, NULL, NULL, NULL, '15:30:29', 'O3b', 0),
(33, '2015181521', 'Jules Margarette', 'A.', 'Andaya', '3rd', NULL, NULL, NULL, NULL, '15:33:38', 'O3B', 0),
(34, '2015170951', 'Jenelyn', 'T.', 'Alday', '3rd', NULL, NULL, NULL, NULL, '15:34:26', 'O3B', 0),
(35, '2014158831', 'Christopher', 'S', 'Espiritu', '4th year', NULL, NULL, NULL, NULL, '15:35:00', 'O3B', 0),
(36, '2015178551', 'Kees Vincent', 'A', 'Ajunan', '3rd Year', NULL, NULL, NULL, NULL, '15:35:42', '03B', 0),
(37, '2015170991', 'Earl Timothy', 'A', 'Ablaza', '3', NULL, NULL, NULL, NULL, '15:36:16', 'O3A', 0),
(38, '2015168881', 'Paolo ', 'S', 'Macatangay', '3rd', NULL, NULL, NULL, NULL, '15:45:48', '03b', 0),
(40, '2014139311', 'Neil Patrick', 'C', 'Anoyo', '4', NULL, NULL, NULL, NULL, '15:55:16', 'O4B', 0),
(41, '2015169911', 'mac anniel ', 'l', 'dimailig', '3rd', NULL, NULL, NULL, NULL, '15:57:46', 'o3b', 0),
(42, '2015167851', 'Maria Edlyne Joyce ', 'E', 'Casao', '3rd', NULL, NULL, NULL, NULL, '16:29:37', 'O3B', 0),
(43, '2015167661', 'Vanessa', 'O', 'Barrion', '3', NULL, NULL, NULL, NULL, '16:30:16', 'O3B', 0),
(44, '2015173241', 'Emmaline Grace', 'S.', 'Lumanglas', '3RD', NULL, NULL, NULL, NULL, '16:30:47', 'O3B', 0),
(45, '2015171031', 'Abigail Rose ', 'P', 'Edlagan ', '3rd', NULL, NULL, NULL, NULL, '16:31:16', 'O3B', 0),
(46, '2015172031', 'Rafaella Franchesca', 'D.', 'Gonzales', '3rd', NULL, NULL, NULL, NULL, '16:31:56', 'O3B', 0),
(53, '2014123456', 'iuh', 'liuh', 'iuyhi', 'liuh', NULL, NULL, NULL, NULL, '23:40:51', 'liuh', 0);

-- --------------------------------------------------------

--
-- Table structure for table `event_3`
--

CREATE TABLE `event_3` (
  `id` int(11) UNSIGNED NOT NULL,
  `Student_Number` varchar(300) NOT NULL,
  `First_Name` varchar(500) NOT NULL,
  `Middle_Initial` varchar(50) NOT NULL,
  `Last_Name` varchar(300) NOT NULL,
  `Year` varchar(300) NOT NULL,
  `Address` varchar(300) DEFAULT NULL,
  `Gender` varchar(300) DEFAULT NULL,
  `Age` varchar(300) DEFAULT NULL,
  `Contact_Number` varchar(300) DEFAULT NULL,
  `Time_of_Arrival` time NOT NULL,
  `Section` varchar(300) NOT NULL,
  `EventID` int(11) DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event_3`
--

INSERT INTO `event_3` (`id`, `Student_Number`, `First_Name`, `Middle_Initial`, `Last_Name`, `Year`, `Address`, `Gender`, `Age`, `Contact_Number`, `Time_of_Arrival`, `Section`, `EventID`) VALUES
(1, '2014141511', 'Marjay', 'P.', 'Tapay', '4th', NULL, NULL, NULL, NULL, '09:14:46', 'O4A', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminaccounts`
--
ALTER TABLE `adminaccounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events_list`
--
ALTER TABLE `events_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_1`
--
ALTER TABLE `event_1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_3`
--
ALTER TABLE `event_3`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adminaccounts`
--
ALTER TABLE `adminaccounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `events_list`
--
ALTER TABLE `events_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `event_1`
--
ALTER TABLE `event_1`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `event_3`
--
ALTER TABLE `event_3`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
