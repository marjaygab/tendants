/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aces;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTimePicker;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author User
 */
public class EventsEditController implements Initializable {
@FXML
    private StackPane stackPane;
@FXML
    private AnchorPane navList;

    @FXML
    private JFXTextField editeventname;

    @FXML
    private JFXDatePicker editdate;

    @FXML
    private JFXTimePicker editstart;

    @FXML
    private JFXTextField editvenue;

    @FXML
    private JFXTimePicker editend;

    @FXML
    private JFXButton editsavebutton;

    @FXML
    private JFXButton editcancelbutton;
     TranslateTransition openNav;
    TranslateTransition closeNav;
    
     @FXML
    void saveButtonPressed(ActionEvent event) {

        
        openNav.play();
         System.out.println("Should played");
        
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        openNav = new TranslateTransition(new Duration(350),navList);
        closeNav  = new TranslateTransition(new Duration(350),navList);
        prepareSlideEffect();
    }    
    
    public void prepareSlideEffect(){
       
        openNav.setToX(0);
        //openNav.play();
        
        
        
        
    
    
    }
    
}
