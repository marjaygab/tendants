/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aces;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXSpinner;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.PauseTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author User
 */
public class EnterDatabaseController implements Initializable {

    /**
     * Initializes the controller class.
     */
  @FXML
    private JFXTextField databasefield;

    @FXML
    private JFXTextField usernamefield;

        @FXML
    private StackPane stackDatabase;
    
    @FXML
    private JFXPasswordField passwordfield;
    @FXML
    private JFXButton connectButton;

    @FXML
    private JFXButton cancelButton;
    private CustomLoader loader;
    @FXML
    void cancelPressed(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    void connecttoDatabase(ActionEvent event) {
        ConnectDB connect = new ConnectDB(databasefield.getText(),usernamefield.getText(),passwordfield.getText());
        
          JFXDialogLayout layout = new JFXDialogLayout();
           // layout.setHeading(new Text("Alert"));
           JFXSpinner spinner = new JFXSpinner();
           Text text = new Text("     Loading..");
           text.setTextAlignment(TextAlignment.RIGHT);
           //spinner.setLayoutX(text.getLayoutX()-spinner.getMaxWidth());
          spinner.setPadding(new Insets(0, 100, 0, 30));
            layout.setBody(spinner,text);
            //layout.setBody(new Text("Loading.."));
           //layout.setBody(new JFXSpinner());
           
            JFXDialog dialog = new JFXDialog(stackDatabase, layout, JFXDialog.DialogTransition.CENTER);        
            dialog.setMaxSize(150, 20);
         // layout.setActions(confirm);
                  
            Text text2 = new Text("Connected!!");
            Text text3 = new Text("Connection Failed");
            JFXDialogLayout layout2 = new JFXDialogLayout();
            JFXDialogLayout layout3 = new JFXDialogLayout();
           // layout.setHeading(new Text("Alert"));
            layout2.setBody(text2);
            layout3.setBody(text3);
            JFXDialog dialog2 = new JFXDialog(stackDatabase, layout2, JFXDialog.DialogTransition.CENTER);
            JFXDialog dialog3 = new JFXDialog(stackDatabase,layout3,JFXDialog.DialogTransition.CENTER);
            JFXButton okayButton = new JFXButton("Okay");
             JFXButton okayButton3 = new JFXButton("Okay");
         layout2.setActions(okayButton);   
         layout3.setActions(okayButton3);
         PauseTransition delay = new PauseTransition(Duration.millis(2500));

        if (connect.isConnected) {
            
              dialog2.setMaxSize(100, 50);
            okayButton.setOnAction((event1) -> {
                dialog2.close();
             Stage stage = (Stage) connectButton.getScene().getWindow();
            loader.loadMain(stage, "Login.fxml");
            });       
             dialog.show();                      
             delay.setOnFinished((event1) -> {               
                 dialog.close();
                 dialog2.show();
             });            
             delay.play();               
        }
        else{
        
        dialog3.setMaxSize(100, 50);
              dialog.show();           
            
             delay.setOnFinished((event1) -> {               
                 dialog.close();
                 dialog3.show();
             });
             
             okayButton3.setOnAction((event2) -> {
                dialog3.close();
              
            });
             layout3.setActions(okayButton3);
             delay.play();      
            
        
        }
        
    }
    
    
 
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        loader = new CustomLoader();
    }    
    
}
