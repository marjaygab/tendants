/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aces;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTimePicker;
/**
 *
 * @author User
 */
public class Events {
    private String first_name,middle_initial,last_name,
            age,name_of_event,student_number;
    
    private JFXDatePicker date_of_event;
    private JFXTimePicker start_of_event,end_of_event;

    public Events(String name_of_event, JFXDatePicker date_of_event, JFXTimePicker start_of_event, JFXTimePicker end_of_event) {
        this.name_of_event = name_of_event;
        this.date_of_event = date_of_event;
        this.start_of_event = start_of_event;
        this.end_of_event = end_of_event;
        
    }

    
    
    
    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMiddle_initial() {
        return middle_initial;
    }

    public void setMiddle_initial(String middle_initial) {
        this.middle_initial = middle_initial;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getName_of_event() {
        return name_of_event;
    }

    public void setName_of_event(String name_of_event) {
        this.name_of_event = name_of_event;
    }

    public String getStudent_number() {
        return student_number;
    }

    public void setStudent_number(String student_number) {
        this.student_number = student_number;
    }

    public JFXDatePicker getDate_of_event() {
        return date_of_event;
    }

    public void setDate_of_event(JFXDatePicker date_of_event) {
        this.date_of_event = date_of_event;
    }

    public JFXTimePicker getStart_of_event() {
        return start_of_event;
    }

    public void setStart_of_event(JFXTimePicker start_of_event) {
        this.start_of_event = start_of_event;
    }

    public JFXTimePicker getEnd_of_event() {
        return end_of_event;
    }

    public void setEnd_of_event(JFXTimePicker end_of_event) {
        this.end_of_event = end_of_event;
    }
    
    
    
    
}
