/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aces;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXSpinner;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.PauseTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author User
 */
public class LoginController implements Initializable {
    
    @FXML
    private JFXButton login_button;
    @FXML
    private JFXTextField username;
    @FXML
    private JFXPasswordField password;
    @FXML
    private StackPane stackPane;
    @FXML
    
   private CustomLoader loader;
    private JFXDialog dialog;
    private JFXDialogLayout layout;
    public static String first,middle,last,position;
    private double x,y;
        @FXML
    private Pane topbarPane;

      @FXML
    private ImageView close_button;

    @FXML
    private ImageView minimize_button;

    @FXML
    void closebutton_pressed(MouseEvent event) {
   
       close_button.setDisable(true);
       minimize_button.setDisable(true);
        JFXDialogLayout layout = new JFXDialogLayout();
           // layout.setHeading(new Text("Alert"));
          JFXButton yes_button = new JFXButton("Yes");
          JFXButton no_button = new JFXButton("No");
          Text text = new Text("Are you sure?");
          text.setTextAlignment(TextAlignment.LEFT);
           //spinner.setLayoutX(text.getLayoutX()-spinner.getMaxWidth());
          layout.setHeading(new Text("Alert"));
          layout.setBody(text);
            //layout.setBody(new Text("Loading.."));
           //layout.setBody(new JFXSpinner());
           
            JFXDialog dialog = new JFXDialog(stackPane, layout, JFXDialog.DialogTransition.CENTER);        
            dialog.setMaxSize(150, 20);
            
           dialog.show();
         
          layout.setActions(yes_button,no_button);
                  
          yes_button.setOnAction((event1) -> {            
               System.exit(0);           
          });
          
        no_button.setOnAction((event2) -> {
            dialog.close();
            close_button.setDisable(false);
            minimize_button.setDisable(false);
        });
          
         
     
        
       
        
        
    }

    

    @FXML
    void minimize_pressed(MouseEvent event) {

        Stage stage = (Stage) ((ImageView) event.getSource()).getScene().getWindow();
        stage.setIconified(true);
     
        
    }
    
    
    
    
    
    @FXML
    private void loginPressed(ActionEvent e) {
        System.err.println("NULL HERE");
        
          JFXDialogLayout layout = new JFXDialogLayout();
           // layout.setHeading(new Text("Alert"));
           JFXSpinner spinner = new JFXSpinner();
           Text text = new Text("     Loading..");
           text.setTextAlignment(TextAlignment.RIGHT);
           //spinner.setLayoutX(text.getLayoutX()-spinner.getMaxWidth());
          spinner.setPadding(new Insets(0, 100, 0, 30));
            layout.setBody(spinner,text);
            //layout.setBody(new Text("Loading.."));
           //layout.setBody(new JFXSpinner());
           
            JFXDialog dialog = new JFXDialog(stackPane, layout, JFXDialog.DialogTransition.CENTER);        
            dialog.setMaxSize(150, 20);
         // layout.setActions(confirm);
                  
            Text text2 = new Text("Log-In Successfull!");
            Text text3 = new Text("Log-in Failed.");
            JFXDialogLayout layout2 = new JFXDialogLayout();
            JFXDialogLayout layout3 = new JFXDialogLayout();
           // layout.setHeading(new Text("Alert"));
            layout2.setBody(text2);
            layout3.setBody(text3);
            JFXDialog dialog2 = new JFXDialog(stackPane, layout2, JFXDialog.DialogTransition.CENTER);
            JFXDialog dialog3 = new JFXDialog(stackPane,layout3,JFXDialog.DialogTransition.CENTER);
            JFXButton okayButton = new JFXButton("Okay");
             JFXButton okayButton3 = new JFXButton("Okay");
         layout2.setActions(okayButton);   
         layout3.setActions(okayButton3);
         PauseTransition delay = new PauseTransition(Duration.millis(2500));
        if (login(username.getText(),password.getText())) {
            System.out.println("Test");
           
             Stage stage = (Stage) login_button.getScene().getWindow();
            dialog2.setMaxSize(100, 50);
            okayButton.setOnAction((event) -> {
                dialog2.close();
              loader.loadMain(stage,"MainMenu.fxml");
            });
            
           
            System.out.println("Success");
       
             dialog.show();           
            
             delay.setOnFinished((event1) -> {               
                 dialog.close();
                 dialog2.show();
             });
             
             delay.play();      
        }
        else{
         dialog3.setMaxSize(100, 50);
              dialog.show();           
            
             delay.setOnFinished((event1) -> {               
                 dialog.close();
                 dialog3.show();
             });
             
             okayButton3.setOnAction((event) -> {
                dialog3.close();
              
            });
             layout3.setActions(okayButton3);
             delay.play();      
        
        }
        
        
        
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    
   loader = new CustomLoader();
         
        topbarPane.setOnMousePressed((event) -> {
        
            Stage stage = (Stage) ((Pane) event.getSource()).getScene().getWindow();
                this.x = stage.getX() - event.getScreenX();
                this.y = stage.getY() - event.getScreenY();    
        });
         topbarPane.setOnMouseDragged((event1) -> {
            Stage stage = (Stage) ((Pane) event1.getSource()).getScene().getWindow();
            stage.setX(event1.getScreenX()+this.x);
            stage.setY(event1.getScreenY()+this.y);
            
        });
   

        // TODO
    }    
 
    
    
    
    public Boolean login(String user, String pass){
    
        Boolean grant = false;
        
        try{
            String query ="SELECT * FROM `adminaccounts`";
            
            ConnectDB.rs=ConnectDB.st.executeQuery(query);
            
            if (ConnectDB.rs==null) {
                System.err.println("NULL!");
            }
            
            while (ConnectDB.rs.next()){
            first = ConnectDB.rs.getString("First_Name");
            middle = ConnectDB.rs.getString("Middle_Initial");
            last = ConnectDB.rs.getString("Last_Name");
            position = ConnectDB.rs.getString("Position");
            String username = ConnectDB.rs.getString("Username");
            String password = ConnectDB.rs.getString("Password");
            
           if (user.equals(username) && pass.equals(password)){
              grant =  true;
             break;
            }
            else if(user!=username && pass!=password){
                
                grant = false;
                
            }
            
            } 
            }catch(SQLException ex){
                 System.err.println(ex);
                 return false;
            }
        
    return grant;
        
        
    }
    
    
}
