/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aces;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author User
 */
public class ConnectDB {
    
    
    public static  Connection con;
    public static  Statement st;
    public static  ResultSet rs;
    public static Boolean isConnected;
    public ConnectDB(String database,String user, String password) {
    try{
        Class.forName("com.mysql.jdbc.Driver");
        con=DriverManager.getConnection("jdbc:mysql://"+database+"/aces", user, password);
        st=con.createStatement(); 
        System.out.println("Success!");
        isConnected = true;
        }catch (Exception ex){
            isConnected = false;
            System.out.println("Error: " + ex);
        }
        
    }   
    
    
    
}
