/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aces;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author User
 */
public class ScheduleFields {
    public static Boolean firstname,timeofarrival,middleinitial,lastname,studnumber,gender,age,contact,address,year,section;
    public static List<Boolean> listofFields;
   private Fields fields;

    public List<Boolean> getListofFields() {
        return listofFields;    
    }
    

    public ScheduleFields(Boolean firstname, Boolean middleinitial, Boolean lastname, Boolean studnumber, Boolean gender, Boolean age, Boolean contact, Boolean address, Boolean year , Boolean time_of_arrival , Boolean section) {
        listofFields = new ArrayList<>();
        this.firstname = firstname;
        this.middleinitial = middleinitial;
        this.lastname = lastname;
        this.studnumber = studnumber;
        this.gender = gender;
        this.age = age;
        this.contact = contact;
        this.address = address;
        this.year = year;
        this.section = section;
        this.timeofarrival = time_of_arrival;
        listofFields.add(studnumber); //0
        listofFields.add(firstname); //1
        listofFields.add(middleinitial); //2
        listofFields.add(lastname); //3
        listofFields.add(year); //4
        listofFields.add(address); //5
        listofFields.add(gender); //6
        listofFields.add(age); //7
        listofFields.add(contact); //8
        listofFields.add(time_of_arrival);//9
        listofFields.add(section);//10
        
    }
    
    

    
    public String getCommandString(){
       StringBuilder sb = new StringBuilder();
        sb.append(fields.ID.getDescription());
        sb.append(",");
      
        
        
        for (int i = 0; i < listofFields.size(); i++) {
            
            switch(i){
                case 0:
                    if (studnumber) {
                        sb.append(fields.STUD_NUMBER.getDescription() + "NOT NULL,");
                  
                    }
                    else{
                    sb.append(fields.STUD_NUMBER.getDescription() + "NULL,");
                    
                    }
                break;
                case 1:
                    if (firstname) {
                        sb.append(fields.FIRST_NAME.getDescription() + "NOT NULL,");
                       
                    }
                    else{
                    sb.append(fields.FIRST_NAME.getDescription() + "NULL,");
                   
                    }
                    
                break;
                case 2:
                    if (middleinitial) {
                        sb.append(fields.MIDDLE_INITIAL.getDescription()+ "NOT NULL,");
                      
                    }
                     else{
                    sb.append(fields.MIDDLE_INITIAL.getDescription() + "NULL,");
                     
                    }
                break;
                case 3:
                    if (lastname) {
                        sb.append(fields.LAST_NAME.getDescription()+"NOT NULL,");
                       
                    }
                     else{
                    sb.append(fields.LAST_NAME.getDescription() + "NULL,");
                    
                    }
                break;
                case 4:
                    if (year) {
                        sb.append(fields.YEAR.getDescription()+"NOT NULL,");
                      
                    }
                     else{
                    sb.append(fields.YEAR.getDescription() + "NULL,");
                     
                    }
                break;
                case 5:
                    if (address) {
                        sb.append(fields.ADDRESS.getDescription()+"NOT NULL,");
                       
                    }
                     else{
                    sb.append(fields.ADDRESS.getDescription() + "NULL,");
                    
                    }
                break;
                case 6:
                    if (gender) {
                        sb.append(fields.GENDER.getDescription()+"NOT NULL,");
                       
                    }
                     else{
                    sb.append(fields.GENDER.getDescription() + "NULL,");
                    
                    }
                break;
                case 7:
                    if (age) {
                        sb.append(fields.AGE.getDescription()+"NOT NULL,");
                      
                    }
                     else{
                    sb.append(fields.AGE.getDescription() + "NULL,");
                     
                    }
                break;
                case 8:
                    if (contact) {
                        sb.append(fields.CONTACT_NUMBER.getDescription() + "NOT NULL,");
                       
                    }
                     else{
                    sb.append(fields.CONTACT_NUMBER.getDescription() + "NULL,");
                     
                    }
                break;
                case 9:
                    if (timeofarrival) {
                        sb.append(fields.TIME_OF_ARRIVAL.getDescription()+"NOT NULL,");
                      
                    }
                     else{
                    sb.append(fields.TIME_OF_ARRIVAL.getDescription() + "NULL,");
                    
                    }
                break;
                
                case 10:
                    if (section) {
                        sb.append(fields.SECTION.getDescription()+"NOT NULL,");
                        
                    }
                     else{
                    sb.append(fields.SECTION.getDescription() + "NULL,");
                   
                    }
                break;
                
                
                default:
                break;
            }
            
            
            
            
        }
        
        sb.append(fields.EVENT_ID.getDescription());
        
        
    
    return sb.toString();
    }
       
    
    public Boolean shouldEnd(int currIndex){
    
        Iterator itr = listofFields.listIterator(currIndex);
        Boolean shouldComma = false;
        while (itr.hasNext()) {
            Object next = itr.next();
            
            if (next.equals(true)) {
            shouldComma = true;
            break;
            }
            
        }
        
    return shouldComma;
    
    }
    
    
}
enum Fields{
    ID("id INT(11)  UNSIGNED AUTO_INCREMENT PRIMARY KEY"),
    FIRST_NAME("First_Name VARCHAR(500) "),
    LAST_NAME("Last_Name VARCHAR(300) "),
    MIDDLE_INITIAL("Middle_Initial VARCHAR(50) "),
    EVENT_NAME("Event VARCHAR(500) "),
    DATE_OF_EVENT("Date_of_Event DATE"),
    TIME_OF_ARRIVAL("Time_of_Arrival TIME "),
    TIME_START("Time_Start TIME "),
    TIME_END("Time_End TIME"),
    EVENT_ID("EventID INT(11)"),
    AGE("Age VARCHAR(300) "),
    GENDER("Gender VARCHAR(300) "),
    CONTACT_NUMBER("Contact_Number VARCHAR(300) "),
    ADDRESS("Address VARCHAR(300) "),
    YEAR("Year VARCHAR(300) "),
    STUD_NUMBER("Student_Number VARCHAR(300) "),
    SECTION("Section VARCHAR(300) ");
    
    private String description;
    
    Fields(String desc){
    this.description = desc;
    }

    public String getDescription() {
        return description;
    }
    
    



}
