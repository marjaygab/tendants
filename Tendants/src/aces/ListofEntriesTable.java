/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aces;

import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.sql.SQLException;
import java.time.LocalTime;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

/**
 *
 * @author User
 */
public class ListofEntriesTable extends RecursiveTreeObject<ListofEntriesTable>{
    
    
    private StringProperty firstname,lastname,middlename,age,gender,address,yearsect,contact,studentnum;
    private SimpleObjectProperty<LocalTime> timeofarrival;
    private ObservableList<ListofEntriesTable> entries = FXCollections.observableArrayList();;
    private JFXTreeTableColumn<ListofEntriesTable,String> firstcolumn,middlecolumn,lastcolumn,agecolumn,gendercolumn,addresscolumn,
            yearsectcolumn,contactcolumn,studentnumcolumn;
    private JFXTreeTableColumn<ListofEntriesTable,LocalTime> timecolumn;
    
    
    
    public ListofEntriesTable(String firstname, String lastname, String middlename, String age, String gender, String address, String yearsect, String contact, String studentnum, LocalTime timeofarrival) {
        this.firstname = new SimpleStringProperty(firstname);
        this.lastname = new SimpleStringProperty(lastname);
        this.middlename = new SimpleStringProperty(middlename);
        this.age = new SimpleStringProperty(age);
        this.gender = new SimpleStringProperty(gender);
        this.address = new SimpleStringProperty(address);
        this.yearsect = new SimpleStringProperty(yearsect);
        this.contact = new SimpleStringProperty(contact);
        this.studentnum = new SimpleStringProperty(studentnum);
        this.timeofarrival = new SimpleObjectProperty<>(timeofarrival);
         
    }

    public ListofEntriesTable() {
        
    }
    
    
    
    public void setColumns(){
    
           firstcolumn = new JFXTreeTableColumn<>("Event");
        firstcolumn.setPrefWidth(150);
       firstcolumn.setCellValueFactory((param) -> {
           if (firstcolumn.validateValue(param)) {
               return param.getValue().getValue().firstname;
           }
           else return firstcolumn.getComputedValue(param);                    
       });
    
     middlecolumn = new JFXTreeTableColumn<>("M.I.");
        middlecolumn.setPrefWidth(150);
       middlecolumn.setCellValueFactory((param) -> {
           if (middlecolumn.validateValue(param)) {
               return param.getValue().getValue().middlename;
           }
           else return middlecolumn.getComputedValue(param);                    
       });
    
        lastcolumn = new JFXTreeTableColumn<>("Last Name");
        lastcolumn.setPrefWidth(150);
       lastcolumn.setCellValueFactory((param) -> {
           if (lastcolumn.validateValue(param)) {
               return param.getValue().getValue().lastname;
           }
           else return lastcolumn.getComputedValue(param);                    
       });
    
       studentnumcolumn = new JFXTreeTableColumn<>("Student Number");
        studentnumcolumn.setPrefWidth(150);
       studentnumcolumn.setCellValueFactory((param) -> {
           if (studentnumcolumn.validateValue(param)) {
               return param.getValue().getValue().studentnum;
           }
           else return studentnumcolumn.getComputedValue(param);                    
       });
       
       yearsectcolumn = new JFXTreeTableColumn<>("Year/Section");
        yearsectcolumn.setPrefWidth(150);
       yearsectcolumn.setCellValueFactory((param) -> {
           if (yearsectcolumn.validateValue(param)) {
               return param.getValue().getValue().yearsect;
           }
           else return yearsectcolumn.getComputedValue(param);                    
       });
       
       agecolumn = new JFXTreeTableColumn<>("Age");
        agecolumn.setPrefWidth(150);
       agecolumn.setCellValueFactory((param) -> {
           if (agecolumn.validateValue(param)) {
               return param.getValue().getValue().age;
           }
           else return agecolumn.getComputedValue(param);                    
       });
       
        gendercolumn = new JFXTreeTableColumn<>("Gender");
        gendercolumn.setPrefWidth(150);
       gendercolumn.setCellValueFactory((param) -> {
           if (gendercolumn.validateValue(param)) {
               return param.getValue().getValue().gender;
           }
           else return gendercolumn.getComputedValue(param);                    
       });
       
       
       contactcolumn = new JFXTreeTableColumn<>("Gender");
        contactcolumn.setPrefWidth(150);
       contactcolumn.setCellValueFactory((param) -> {
           if (contactcolumn.validateValue(param)) {
               return param.getValue().getValue().contact;
           }
           else return contactcolumn.getComputedValue(param);                    
       });
       
       
        timecolumn = new JFXTreeTableColumn<>("Time of Arrival");
        timecolumn.setPrefWidth(150);
       timecolumn.setCellValueFactory((param) -> {
           if (timecolumn.validateValue(param)) {
               return param.getValue().getValue().timeofarrival;
           }
           else return timecolumn.getComputedValue(param);                    
       });
        
         
        addresscolumn = new JFXTreeTableColumn<>("Address");
        addresscolumn.setPrefWidth(150);
       addresscolumn.setCellValueFactory((param) -> {
           if (addresscolumn.validateValue(param)) {
               return param.getValue().getValue().address;
           }
           else return addresscolumn.getComputedValue(param);                    
       });
       
        
        
    }
    
   public JFXTreeTableView buildTree(JFXTreeTableView table){
       table = new JFXTreeTableView();
    final TreeItem<ListofEntriesTable> root = new RecursiveTreeItem<ListofEntriesTable>(entries,RecursiveTreeObject::getChildren);
       // treeTable = new JFXTreeTableView<myEvents>(root);
       table.setRoot(root);
        System.out.println("Mark 1");
        
       table.getColumns().setAll(timecolumn,studentnumcolumn,lastcolumn,firstcolumn,middlecolumn,yearsectcolumn,gendercolumn,agecolumn,addresscolumn,contactcolumn);
        //System.out.println("Mark 2");
        table.setShowRoot(false);
        table.setEditable(true);
  
        return table;
    }
    
   
        public void initializeTable(String eventid){
            eventid = "1013";
             try {                             
           String query = "SELECT * FROM event_"+eventid;
                //PreparedStatement pr = ConnectDB.con.prepareStatement(query);
                //pr.setString(1, account);
             ConnectDB.rs = ConnectDB.st.executeQuery(query);
             
                while (ConnectDB.rs.next()) {                    
                  entries.add(new ListofEntriesTable(ConnectDB.rs.getString("First_Name"), ConnectDB.rs.getString("Last_Name"), ConnectDB.rs.getString("Middle_Initial"), ConnectDB.rs.getString("Age"), ConnectDB.rs.getString("Gender"), 
                          ConnectDB.rs.getString("Address"), ConnectDB.rs.getString("Year_Section"), ConnectDB.rs.getString("Contact_Number"), ConnectDB.rs.getString("Student_Number"), ConnectDB.rs.getTime("Time_of_Arrival").toLocalTime()));
                }
       
               
        } catch (SQLException e) {
            System.out.println("Error: " + e);
        }
                 
        } 
   
   
    
    
}


