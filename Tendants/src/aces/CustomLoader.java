/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aces;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author User
 */
public class CustomLoader {
    
    
     public void loadMain(Stage previous , String path ){
    
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(path));
            Parent root2 = (Parent) loader.load();
            
            Stage stage = new Stage();
            stage.setScene(new Scene(root2));
            previous.close();
            stage.initStyle(StageStyle.UNDECORATED);
            stage.show();
            
        } catch (Exception ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
      }
        
    }
     
     public Stage setFXMLPath(Stage emptystage,String path){
     
         try {
              FXMLLoader loader = new FXMLLoader(getClass().getResource(path));
            Parent root2 = (Parent) loader.load();
            
           // emptystage = new Stage();
            emptystage.setScene(new Scene(root2));    
     
            
            
         } catch (Exception e) {
             System.err.println("Error 102: " + e);
             
         }
     
         return emptystage;
     }
     
     public void hideWindow(Stage previous , String path){
     
         try {
             
             
             FXMLLoader loader = new FXMLLoader(getClass().getResource(path));
            Parent root2 = (Parent) loader.load();
           
            Stage stage = new Stage();
            stage.setScene(new Scene(root2));
            previous.close();
            stage.close();
             
         } catch (Exception e) {
             System.err.println("Error 101: " + e);
         }
         
     }


    
}
