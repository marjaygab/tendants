/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aces;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXPopup;
import com.jfoenix.controls.JFXSpinner;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTimePicker;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableRow;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.cells.editors.TextFieldEditorBuilder;
import com.jfoenix.controls.cells.editors.base.EditorNodeBuilder;
import com.jfoenix.controls.cells.editors.base.GenericEditableTreeTableCell;
import com.jfoenix.controls.cells.editors.base.JFXTreeTableCell;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.awt.Color;

import java.awt.event.MouseEvent;
import java.net.URL;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javafx.animation.TranslateTransition;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableRow;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Background;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.util.Callback;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javafx.scene.control.DatePicker;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;
/**
 * FXML Controller class
 *
 * @author User
 */
public class EventsMenuController implements Initializable {
    
    
@FXML
    private JFXTextField name_of_event;

    @FXML
    private JFXDatePicker date_of_event;

       @FXML
    private JFXCheckBox section_check;

    @FXML
    private JFXTimePicker start_of_event;

    @FXML
    private JFXTimePicker end_of_event;

    @FXML
    private JFXCheckBox first_check;

      @FXML
    private ImageView backButton;

    @FXML
    private JFXCheckBox middle_check;

    @FXML
    private JFXCheckBox last_check;

    @FXML
    private JFXCheckBox gender_check;

    @FXML
    private JFXCheckBox stud_num_check;

    
    @FXML
    private Pane windowPane;
    
    @FXML
    private JFXCheckBox age_check;

    @FXML
    private JFXCheckBox contact_check;

    @FXML
    private JFXCheckBox address_check;

    @FXML
    private JFXCheckBox year_check;

    @FXML
    private JFXCheckBox time_of_arrival_check;

    @FXML
    private JFXButton save_button;

    @FXML
    private JFXButton clear_button;

        @FXML
    private JFXTextField venue_of_event;
    
 @FXML
    private JFXTreeTableView<myEvents> treeTable; 
    
  @FXML
    private JFXTextField searchField;
  
    @FXML
    private JFXTextField editname;

    @FXML
    private JFXTextField editvenue;

    @FXML
    private JFXTimePicker editstart;

    @FXML
    private JFXTimePicker editend;

    @FXML
    private JFXDatePicker editdate;

    @FXML
    private JFXButton editupdatebutton;

    @FXML
    private JFXButton editcancelbutton;
 @FXML
    private AnchorPane anchorPaneToast;
   @FXML
    private StackPane stackPane2;

     @FXML
    private StackPane stackEventPane;
    private JFXButton b1,b2,b3;
     private JFXListView<JFXButton> popupList;
    private JFXPopup popup; 
    private ObservableList<myEvents> eventslist;
    private JFXTreeTableColumn<myEvents,String> eventName,venueName,eventID;
    private JFXTreeTableColumn<myEvents,Date> dateActivity;
    private JFXTreeTableColumn<myEvents,LocalTime> timeStart,timeEnd;
    private Time start,end;
    private Date dateofevent;
    private final Paint ripplerFill = Paint.valueOf("008F82");
    private int mouseClicks,previousIndex,temp;
    private String eventIDSelected;
    private TextFieldEditorBuilder builder;
    private EditorNodeBuilder<JFXDatePicker> datebuilder;
    private CustomLoader loader;
    private TranslateTransition openEdit,closeEdit,menuattendopen,menuattendclose;
    public static String selectedEventId;
    private FXMLLoader loader1;
    public static ScheduleFields fields;
    private double x,y;
    
    private Callback<TreeTableColumn<myEvents,Date>,TreeTableCell<myEvents,Date>> dateCellFactory;
    @FXML
    void clearButtonPressed(ActionEvent event) {
            findMaxID();
             //eventName.setVisible(false);
          // MainMenuController.lastattendancefield.setEditable(false);
              
     
            
    }
    
      @FXML
    void cancelButtonPressed(ActionEvent event) {
        
         openEdit.play();
         //MainMenuController.lastattendancefield.setEditable(false);
         // System.out.println("Text Field Editable False");
        
    }
    
    

    
     @FXML
    void updateButtonPressed(ActionEvent event) {

//         editname.getText(treeTable.getSelectionModel().getSelectedItem().getValue().eventname.getValue());
//         editvenue.getText(treeTable.getSelectionModel().getSelectedItem().getValue().venue.getValue());
//         editstart.getValue(treeTable.getSelectionModel().getSelectedItem().getValue().timestart.getValue());
//         editend.getValue(treeTable.getSelectionModel().getSelectedItem().getValue().timeend.getValue());
//         editdate.getValue(treeTable.getSelectionModel().getSelectedItem().getValue().datetime.getValue().toLocalDate());
        treeTable.getSelectionModel().getSelectedItem().getValue().eventname.setValue(editname.getText());
        treeTable.getSelectionModel().getSelectedItem().getValue().venue.setValue(editvenue.getText());
        treeTable.getSelectionModel().getSelectedItem().getValue().timestart.setValue(editstart.getValue());
        treeTable.getSelectionModel().getSelectedItem().getValue().timeend.setValue(editend.getValue());
        treeTable.getSelectionModel().getSelectedItem().getValue().datetime.setValue(Date.valueOf(editdate.getValue()));
        
        
        
         updateTable("EventName", treeTable.getSelectionModel().getSelectedItem().getValue().eventid.getValue(), treeTable.getSelectionModel().getSelectedItem().getValue().eventname.getValue());
        updateTable("Venue", treeTable.getSelectionModel().getSelectedItem().getValue().eventid.getValue(), treeTable.getSelectionModel().getSelectedItem().getValue().venue.getValue());
         updateTable("Date", treeTable.getSelectionModel().getSelectedItem().getValue().eventid.getValue(), treeTable.getSelectionModel().getSelectedItem().getValue().datetime.getValue());
        updateTable("TimeStart", treeTable.getSelectionModel().getSelectedItem().getValue().eventid.getValue(), Time.valueOf(treeTable.getSelectionModel().getSelectedItem().getValue().timestart.getValue()));
        updateTable("TimeEnd", treeTable.getSelectionModel().getSelectedItem().getValue().eventid.getValue(), Time.valueOf(treeTable.getSelectionModel().getSelectedItem().getValue().timeend.getValue()));
        
        JFXDialogLayout layout2 = new JFXDialogLayout();
           // layout.setHeading(new Text("Alert"));
            layout2.setBody(new Text("Update Successful"));
           
            JFXDialog dialog = new JFXDialog(stackPane2, layout2, JFXDialog.DialogTransition.CENTER);
            JFXButton okayButton = new JFXButton("Okay");
            
            dialog.setMaxSize(100, 50);
            okayButton.setOnAction((event2) -> {
                dialog.close();
              openEdit.play();
            });
            
            layout2.setActions(okayButton);
            
            dialog.show();
    
        
    }
    
    
    
     @FXML
    void saveButtonPressed(ActionEvent event) {
         System.out.println("Test");
         //String eventtablename = name_of_event.getText().toString().toLowerCase().replace(" ", "_");
         String eventID = getAutoGenID();
          fields = new ScheduleFields(first_check.isSelected(), middle_check.isSelected(), last_check.isSelected(),stud_num_check.isSelected(), 
                gender_check.isSelected(), age_check.isSelected(), contact_check.isSelected(), address_check.isSelected(), year_check.isSelected(),time_of_arrival_check.isSelected(),section_check.isSelected());
          System.out.println(fields.getCommandString());
          
          String eventtablename =   "event_" + eventID;
          start = Time.valueOf(start_of_event.getValue());
          end = Time.valueOf(end_of_event.getValue());
          dateofevent = Date.valueOf(date_of_event.getValue());
            insertToTable();
         System.out.println(fields.getCommandString());
          
          createTable(fields.getCommandString(), eventtablename,eventID);
          
         System.out.println(eventtablename);

        
    }
    
    public void initializePopup(){
         popup = new JFXPopup(popupList);
        Insets insets = new Insets(10);
        popupList = new JFXListView<>();
         b1 = new JFXButton("Set as Main Event");
         b2 = new JFXButton("           Edit           ");
         b3 = new JFXButton("         Delete         ");
        b1.setPadding(insets);
        b2.setPadding(insets);
        b3.setPadding(insets);
        b3.setRipplerFill(ripplerFill);
        b2.setRipplerFill(ripplerFill);
        b1.setRipplerFill(ripplerFill);
        VBox box = new VBox(b1,b2,b3);
//        popupList.getItems().add(b1);       
//        popupList.getItems().add(b2);       
//        popupList.getItems().add(b3);
        popup.setPopupContent(box);
       

       
        
       
        
        
    
    
    }
    
  
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {        
        System.out.println("Initialized");
       //selectedEventId = "";
        eventslist = FXCollections.observableArrayList();
        mouseClicks=0;
        previousIndex = 0;
        setColumnsonTable();
        initializeTable();
        buildTree();
        //eventIDSelected = "";
      stud_num_check.setSelected(true);
      stud_num_check.setDisable(true);
      time_of_arrival_check.setDisable(true);
        //System.out.println(controller.toString());
        loader = new CustomLoader();
        initializePopup();
        openEdit = new TranslateTransition(new Duration(350),anchorPaneToast);
        openEdit.setToY(0);
        
        closeEdit = new TranslateTransition(new Duration(350),anchorPaneToast);
        menuattendopen = new TranslateTransition(new Duration(350),MainMenuController.staticAnchor);
        menuattendopen.setToX(0);
        
        menuattendclose = new TranslateTransition(new Duration(350),MainMenuController.staticAnchor);
        builder = new TextFieldEditorBuilder();       
            searchField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
              treeTable.setPredicate(new Predicate<TreeItem<myEvents>>() {
                         @Override
                         public boolean test(TreeItem<myEvents> t) {
                             Boolean flag = t.getValue().eventname.getValue().toUpperCase().startsWith(newValue.toUpperCase());                    
                return flag;
            
            }
                    });                              
            }
                     });           
            treeTable.setRowFactory((param) -> {
               TreeTableRow<myEvents> row = new JFXTreeTableRow<>();
                TreeTableCell<myEvents,String> cell = new JFXTreeTableCell<>();
                
                row.setOnMouseClicked((event) -> {
                     if (event.getButton().equals(MouseButton.SECONDARY)) {
                        popup.show(row, JFXPopup.PopupVPosition.TOP, JFXPopup.PopupHPosition.LEFT , event.getX(),event.getY());
                        b1.setOnAction((e1) -> {
                            try {
                                //put Select Main Event event here
                                selectedEventId = treeTable.getSelectionModel().getSelectedItem().getValue().eventid.getValue();
                                System.out.println(selectedEventId);
                                //hidewindow
                                Stage stage = (Stage) treeTable.getScene().getWindow();
                                //stage.close();
                                 
                                loader.loadMain(stage, "MainMenu.fxml");
                                
                               
//                                closeEdit.setToX(-(MainMenuController.staticAnchor.getWidth()));
//                                closeEdit.play();
                                        
                                        
                                
                            } catch (Exception e) {
                                System.err.println(e);
                            }                                                      
                            //System.out.println("Selected Event: " + treeTable.getSelectionModel().getSelectedItem().getValue().eventid.getValue() );                      
                        });
                        b2.setOnAction((e2) -> {
                            try {
                                //put Edit Event event here
                                System.out.println("Edit");
                                //treeTable.setVisible(false);
                                 //if(anchorPaneToast.getTranslateY()!=0){
                                 
                               popup.hide();
                                 editname.clear();
                                 editvenue.clear();
                                 
                                 editname.setText(treeTable.getSelectionModel().getSelectedItem().getValue().eventname.getValue());
                                 editvenue.setText(treeTable.getSelectionModel().getSelectedItem().getValue().venue.getValue());
                                  editstart.setValue(treeTable.getSelectionModel().getSelectedItem().getValue().timestart.getValue());
                                 editend.setValue(treeTable.getSelectionModel().getSelectedItem().getValue().timeend.getValue());
                                 editdate.setValue(treeTable.getSelectionModel().getSelectedItem().getValue().datetime.getValue().toLocalDate());
                                 
                                 
                                 //}else{
                                closeEdit.setToY(-(anchorPaneToast.getHeight()));
                                closeEdit.play();
                                //}
                                
                                
                                //loader.loadMain(ACES.events, "EventsEdit.fxml");
                                cell.setEditable(true);
                            } catch (Exception e) {
                                System.err.println(e);
                            }
                        });
                        b3.setOnAction((e3) -> {
                            try {
                                //put Delete Event event here
                                selectedEventId = treeTable.getSelectionModel().getSelectedItem().getValue().eventid.getValue();
                                deleteEntry(selectedEventId);
                               
                                //deleteRow();
                                
                                
                            } catch (Exception e) {
                            }
                        });
                    }
                });                                
                return row; //To change body of generated lambdas, choose Tools | Templates.
            });
            
              windowPane.setOnMousePressed((event) -> {
        
            Stage stage = (Stage) ((Pane) event.getSource()).getScene().getWindow();
                this.x = stage.getX() - event.getScreenX();
                this.y = stage.getY() - event.getScreenY();    
        });
        windowPane.setOnMouseDragged((event1) -> {
            Stage stage = (Stage) ((Pane) event1.getSource()).getScene().getWindow();
            stage.setX(event1.getScreenX()+this.x);
            stage.setY(event1.getScreenY()+this.y);
            
        });
  
        backButton.setOnMousePressed((event) -> {
            
              Stage stage = (Stage) (((ImageView) event.getSource()).getScene().getWindow());
        stage.close();      
        loader.loadMain(stage,"MainMenu.fxml");
            
            
        });
        
        
            
      }
    
    
    public void deleteEntry(String id){
    
        try {
            String query = "DELETE from events_list WHERE id ="+ id;
            ConnectDB.st.execute(query);
            
            query = "DROP TABLE event_"+id;
            ConnectDB.st.execute(query);
            
               JFXDialogLayout layout = new JFXDialogLayout();
           // layout.setHeading(new Text("Alert"));
            layout.setBody(new Text("Are you sure you want \n to delete this event?"));
           //layout.setBody(new JFXSpinner());
            JFXDialog dialog = new JFXDialog(stackEventPane, layout, JFXDialog.DialogTransition.CENTER);
            JFXButton confirm = new JFXButton("Yes");
            JFXButton cancel = new JFXButton("Cancel");
            dialog.setMaxSize(100, 50);
           
            confirm.setOnAction((e) -> {
                dialog.close();
                eventslist.clear();
                initializeTable();
                //treeTable.getRoot().getChildren().add(new TreeItem<myEvents>(new myEvents(name_of_event.getText().toString(),venue_of_event.getText().toString(), start.toLocalTime(), end.toLocalTime(), dateofevent,getAutoGenID())));              
            });
            cancel.setOnAction((event3) -> {
            
                dialog.close();
                
            });
            
            layout.setActions(confirm,cancel);
            
            dialog.show();

            
            
            
        } catch (Exception e) {
            
            System.err.println("Error [DeleteQuery]" + e);
            
        }
    
    
    }
    
    
public String getAutoGenID(){
 int maxID = 0;
    try {
        String query = "SELECT Auto_increment FROM information_schema.tables WHERE table_name = 'events_list'";
         ConnectDB.rs = ConnectDB.st.executeQuery(query);
        
         while (ConnectDB.rs.next()) {
                maxID = ConnectDB.rs.getInt(1);
            }
        
        
    } catch (Exception e) {
        System.err.println(e);
    }

        return String.valueOf(maxID);
    

}     


    
    
    
    public void setColumnsonTable(){
    
         eventName = new JFXTreeTableColumn<>("Event");
        eventName.setPrefWidth(150);
       eventName.setCellValueFactory((param) -> {
           if (eventName.validateValue(param)) {
               return param.getValue().getValue().eventname;
           }
           else return eventName.getComputedValue(param);                    
       });
       
          venueName = new JFXTreeTableColumn<>("Venue");
        venueName.setPrefWidth(150);
       venueName.setCellValueFactory((param) -> {
           if (venueName.validateValue(param)) {
               return param.getValue().getValue().venue;
           }
           else return venueName.getComputedValue(param);                    
       });
       
         timeStart = new JFXTreeTableColumn<>("Time Start");
        timeStart.setPrefWidth(150);
       timeStart.setCellValueFactory((param) -> {
           if (timeStart.validateValue(param)) {
               return param.getValue().getValue().timestart;
               
           }
           else return timeStart.getComputedValue(param);                    
       });
       
          timeEnd = new JFXTreeTableColumn<>("Time End");
        timeEnd.setPrefWidth(150);
       timeEnd.setCellValueFactory((param) -> {
           if (timeEnd.validateValue(param)) {
               return param.getValue().getValue().timeend;
           }
           else return timeEnd.getComputedValue(param);                    
       });
       
          dateActivity = new JFXTreeTableColumn<>("Date of Activity");
        dateActivity.setPrefWidth(150);
        dateActivity.setCellValueFactory((param) -> {
            if (dateActivity.validateValue(param)) {
                return param.getValue().getValue().datetime;
            }
            
            else return dateActivity.getComputedValue(param); //To change body of generated lambdas, choose Tools | Templates.
        });
        
        //dateActivity.setCellFactory(dateCellFactory);
        
//       dateActivity.setCellValueFactory((param) -> {
//           if (dateActivity.validateValue(param)) {
//               return param.getValue().getValue().date;
//           }
//           else return dateActivity.getComputedValue(param);                    
//       });
       
       eventID = new JFXTreeTableColumn<>("Event ID");
        eventID.setPrefWidth(150);
       eventID.setCellValueFactory((param) -> {
           if (eventID.validateValue(param)) {
               return param.getValue().getValue().eventid;
           }
           else return eventID.getComputedValue(param);                    
       });
       
       
    
       eventName.setCellFactory((TreeTableColumn<myEvents, String> param) -> new GenericEditableTreeTableCell<myEvents,String>(builder));
       eventName.setOnEditCommit((event) -> {
           
          ((myEvents) event.getTreeTableView().getTreeItem(event.getTreeTablePosition().getRow()).getValue()).eventname.set(event.getNewValue());
          
           updateTable("EventName",((myEvents) event.getTreeTableView().getTreeItem(event.getTreeTablePosition().getRow()).getValue()).eventid.getValue(), event.getNewValue());
          
       });
       
       
       
    
    
    }
    
    public void updateTable(String colName,String id,String value){
    
        System.out.println("Column Name: " + colName);        
        System.out.println("Event ID: " + id);       
        System.out.println("Event Name: " + value);
               
        try {
           String query = "UPDATE events_list SET " + colName + " = ?  WHERE id=?";
                 PreparedStatement st = ConnectDB.con.prepareStatement(query);                 
                 st.setString(1, value);               
                 st.setInt(2, Integer.parseInt(id));                
                 st.executeUpdate();        
        } catch (Exception e) {
        }
        
        
    
    }
    
    public void updateTable(String colName,String id,Date value){
    
        System.out.println("Column Name: " + colName);        
        System.out.println("Event ID: " + id);       
        System.out.println("Event Name: " + value);
               
        try {
           String query = "UPDATE events_list SET " + colName + " = ?  WHERE id=?";
                 PreparedStatement st = ConnectDB.con.prepareStatement(query);                 
                 st.setDate(1, value);               
                 st.setInt(2, Integer.parseInt(id));                
                 st.executeUpdate();        
        } catch (Exception e) {
        }
        
        
    
    }
    
    public void updateTable(String colName,String id,Time value){
    
          System.out.println("Column Name: " + colName);        
        System.out.println("Event ID: " + id);       
        System.out.println("Event Name: " + value);
               
        try {
           String query = "UPDATE events_list SET " + colName + " = ?  WHERE id=?";
                 PreparedStatement st = ConnectDB.con.prepareStatement(query);                 
                 st.setTime(1, value);               
                 st.setInt(2, Integer.parseInt(id));                
                 st.executeUpdate();        
        } catch (Exception e) {
            System.err.println(e);
        }
        
    
    
    }
    
    public void buildTree(){
    final TreeItem<myEvents> root = new RecursiveTreeItem<myEvents>(eventslist,RecursiveTreeObject::getChildren);
       // treeTable = new JFXTreeTableView<myEvents>(root);
       treeTable.setRoot(root);
        System.out.println("Mark 1");
        
       treeTable.getColumns().setAll(eventName,dateActivity,venueName,timeStart,timeEnd,eventID);
        //System.out.println("Mark 2");
        treeTable.setShowRoot(false);
        treeTable.setEditable(true);
  
    
    }
    
    

        public void initializeTable(){
            
             try {                             
           String query = "SELECT * FROM `events_list`";
                //PreparedStatement pr = ConnectDB.con.prepareStatement(query);
                //pr.setString(1, account);
             ConnectDB.rs = ConnectDB.st.executeQuery(query);
             
                while (ConnectDB.rs.next()) {                    
                  eventslist.add(new myEvents(ConnectDB.rs.getString("EventName"),ConnectDB.rs.getString("Venue"),
                  ConnectDB.rs.getTime("TimeStart").toLocalTime(),ConnectDB.rs.getTime("TimeEnd").toLocalTime(),
                  ConnectDB.rs.getDate("Date"),String.valueOf(ConnectDB.rs.getInt("id"))));
                    System.out.println("Added Something");
                }
       
               
        } catch (SQLException e) {
            System.out.println("Error: " + e);
        }
                 
        }


        
        public void insertToTable(){
       
        try {
             String query = "INSERT INTO `events_list`(EventName,Date,Venue,TimeStart,TimeEnd) VALUES (?,?,?,?,?)";
              PreparedStatement state = ConnectDB.con.prepareStatement(query);
            state.setString(1, name_of_event.getText().toString());
            state.setDate(2, dateofevent);
            state.setString(3, venue_of_event.getText().toString());
            state.setTime(4,start);
            state.setTime(5, end);
            state.execute();
              
               JFXDialogLayout layout = new JFXDialogLayout();
           // layout.setHeading(new Text("Alert"));
            layout.setBody(new Text("Success!"));
           //layout.setBody(new JFXSpinner());
            JFXDialog dialog = new JFXDialog(stackEventPane, layout, JFXDialog.DialogTransition.CENTER);
            JFXButton confirm = new JFXButton("Okay");
            
            dialog.setMaxSize(100, 50);
           
            confirm.setOnAction((e) -> {
                dialog.close();
                eventslist.clear();
                initializeTable();
                //treeTable.getRoot().getChildren().add(new TreeItem<myEvents>(new myEvents(name_of_event.getText().toString(),venue_of_event.getText().toString(), start.toLocalTime(), end.toLocalTime(), dateofevent,getAutoGenID())));              
            });
            
            layout.setActions(confirm);
            
            dialog.show();
            
           
            
            
              
         } catch (Exception e) {
             System.err.println("Error here: "+ e);
         }
        
        }
        
        public String findMaxID(){
             int maxId = 0;
            
            try {
                 String getId = "SELECT MAX(id) FROM `events_list`";
            
            ConnectDB.rs = ConnectDB.st.executeQuery(getId);
            
            while (ConnectDB.rs.next()) {
                maxId = ConnectDB.rs.getInt(1);
            }
            
            System.out.println(maxId);
            } catch (Exception e) {
                System.err.println(e);
            }
        
        return String.valueOf(maxId+1);
        
        }


        public void createTable(String command,String tableName,String eventID){
            try {
                String query = "CREATE TABLE "+ tableName + " ("+command+")";
                ConnectDB.st.execute(query);
                System.out.println("Success");
                
                String query1 = "ALTER TABLE `"+tableName+"` CHANGE `EventID` `EventID` INT(11) NULL DEFAULT '"+eventID+"'";
                ConnectDB.st.execute(query1);
                System.out.println("Success in setting default to: "+ eventID);
                
            } catch (Exception e) {
                System.err.println(e);
            }
        
        
        
        }





        }

    class myEvents extends RecursiveTreeObject<myEvents>{
        StringProperty eventname,venue,date,eventid;
        SimpleObjectProperty<Date> datetime;
        SimpleObjectProperty<LocalTime> timestart,timeend;
        public myEvents(String eventname, String venue, LocalTime timestart, LocalTime timeend, Date datetime,String eventid) {
            this.eventname = new SimpleStringProperty(eventname);
            this.venue = new SimpleStringProperty(venue);
            this.timestart = new SimpleObjectProperty<LocalTime>(timestart);
            this.timeend = new SimpleObjectProperty<LocalTime>(timeend);
            this.datetime = new SimpleObjectProperty<Date>(datetime);
            this.eventid = new SimpleStringProperty(eventid);
        }
    
    }
     