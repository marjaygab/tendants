/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aces;

import static aces.ScheduleFields.listofFields;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXSpinner;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableRow;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javafx.animation.KeyFrame;
import javafx.animation.PauseTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableRow;
import javafx.scene.control.TreeTableView;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.util.Duration;
import javax.swing.Timer;
import org.apache.commons.collections4.list.AbstractLinkedList;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * FXML Controller class
 *
 * @author User
 */
public class MainMenuController implements Initializable {
   @FXML
    private StackPane stackMain;

    @FXML
    private Pane windowPane;
      @FXML
    private AnchorPane anchorMainMenu;
    @FXML
    private Text name_of_user;
   @FXML
    private AnchorPane anchorPaneAttendance;
    @FXML
    private Text position;
    @FXML
    private Text idplaceholder;
    @FXML
    private JFXButton EventsObject_button;
   @FXML
    private Text noeventlabel;
    @FXML
    private JFXTextField searchAttendance;
        @FXML
    private JFXTextField sectionattendancefield;
    @FXML
    private JFXTreeTableView<ListOfEntries> listofEntries;
     private TranslateTransition openEdit,closeEdit;
     @FXML
    private JFXButton log_outbutton;
     
    private CustomLoader loader;
    public static AnchorPane staticAnchor;
    private String selected;
    private ListofEntriesTable table;
    private final String filename = "C:/xampp/";
    private String xlName;
     private final String[] colNames = {"Student_Number","First_Name","Middle_Initial","Last_Name","Year","Address","Gender","Age","Contact_Number","Section","Time_of_Arrival"};
    private ObservableList<ListOfEntries> entries;
    private JFXTreeTableColumn<ListOfEntries,String> firstcolumn,middlecolumn,lastcolumn,agecolumn,gendercolumn,addresscolumn,
            yearsectcolumn,contactcolumn,studentnumcolumn,sectioncolumn;
    private JFXTreeTableColumn<ListOfEntries,LocalTime> timecolumn;
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private  FileChooser chooser;
   private double x,y;
    @FXML
    private JFXTextField lastattendancefield;

    @FXML
    private JFXTextField firstatendancefield;

    @FXML
    private JFXTextField middleattendancefield;

    @FXML
    private JFXTextField studentfattendancefield;

    @FXML
    private JFXTextField yearsectattendancefield;

    @FXML
    private JFXTextField genderattendancefield;

    @FXML
    private JFXTextField ageattendancefield;

    @FXML
    private JFXTextField contactattendancefield;

    @FXML
    private JFXTextField addressattendancefield;

    private File file,filedialog;
      @FXML
    private JFXButton exportButton;
private  FileChooser.ExtensionFilter filter;
      
  @FXML
    void log_outbuttonPressed(ActionEvent event) {

          JFXDialogLayout layout = new JFXDialogLayout();
           // layout.setHeading(new Text("Alert"));       
           Text text = new Text("Are you sure?");
           text.setTextAlignment(TextAlignment.RIGHT);
           //spinner.setLayoutX(text.getLayoutX()-spinner.getMaxWidth());        
            layout.setBody(text);
            //layout.setBody(new Text("Loading.."));
           //layout.setBody(new JFXSpinner());      
            JFXDialog dialog = new JFXDialog(stackMain, layout, JFXDialog.DialogTransition.CENTER);
            JFXButton confirm = new JFXButton("Yes");
            JFXButton cancel = new JFXButton("Cancel");
            dialog.setMaxSize(150, 20);
         // layout.setActions(confirm);
             dialog.show();
            
             layout.setActions(confirm,cancel);
           
             confirm.setOnAction((event1) -> {
             
                 Stage stage = (Stage) log_outbutton.getScene().getWindow();
                 stage.close();                
                 loader.loadMain(stage, "Login.fxml");       
                 dialog.close();
             });
             
             cancel.setOnAction((event2) -> {
                 
                 dialog.close();
                 
                 
             });
        
        
    }





    @FXML
    void exporttoXL(ActionEvent event) {
        Node source = (Node) event.getSource();
        Window stage = source.getScene().getWindow();
        
        int count = 0;
        int colCount = 0;
        
       // int size = entries.size()/getVisibleColumns();
        
       // System.out.println(size);
       
         chooser = new FileChooser();
       filter = new FileChooser.ExtensionFilter("EXCEL Files (*.xlsx)","*.xlsx");
        chooser.getExtensionFilters().add(filter);
        
         filedialog = chooser.showSaveDialog(stage);
      
         
         if (filedialog != null) {
            
             for(int j=0;j<1;j++){
       
       Row r = sheet.createRow(count++);
       int colHeaderCount = 0;
        for (int i = 0; i < listofEntries.getColumns().size(); i++) {
            TreeTableColumn col = listofEntries.getColumns().get(i);
          
            if (col.isVisible()) {
              Cell cell = r.createCell(colHeaderCount++);               
                cell.setCellValue(col.getText());
            
            
            }
        }
       }
       
      for (int i = 0; i < entries.size(); i++) {
      Row row = sheet.createRow(count++);
       int colNum = 0;        
      for (int j = 0; j < listofEntries.getColumns().size(); j++) {
          
          
                TreeTableColumn ttc = listofEntries.getColumns().get((j));
                TreeTableView view = ttc.getTreeTableView();           
                
                if (ttc.isVisible()) {
                    Cell cell = row.createCell(colNum++);
                    String value = ttc.getCellData(i).toString();
                    cell.setCellValue(value);
                    System.out.println(value);
                    
                    colCount++;
                
                }
                   
                
      }
      
       
        }
    
         for (int j = 0; j < colCount; j++) {              
              sheet.autoSizeColumn(j);              
         }


             JFXDialogLayout layout = new JFXDialogLayout();
           // layout.setHeading(new Text("Alert"));
           JFXSpinner spinner = new JFXSpinner();
           Text text = new Text("     Exporting..");
           text.setTextAlignment(TextAlignment.RIGHT);
           //spinner.setLayoutX(text.getLayoutX()-spinner.getMaxWidth());
          spinner.setPadding(new Insets(0, 100, 0, 30));
            layout.setBody(spinner,text);
            //layout.setBody(new Text("Loading.."));
           //layout.setBody(new JFXSpinner());
           
            JFXDialog dialog = new JFXDialog(stackMain, layout, JFXDialog.DialogTransition.CENTER);
            JFXButton confirm = new JFXButton("Okay");
            
            dialog.setMaxSize(150, 20);
         // layout.setActions(confirm);
             dialog.show();
             writetoXL();
             PauseTransition delay = new PauseTransition(Duration.millis(2500));
             delay.setOnFinished((event1) -> {
                
                 dialog.close();
                 
             });
             
             delay.play();
            
             
             
        }
         
         
         
         
       
              
      
    }

    public void writetoXL(){
    
        try {
            
              FileOutputStream outStr = new FileOutputStream(filedialog);
              workbook.write(outStr);
              outStr.close();
              //dialog.close();
              System.out.println("Success!");
            
            
        } catch (Exception e) {
        }
    
    
    }
    
   
    public int getVisibleColumns(){
     int colCount = 0;
        
        for (int i = 0; i < listofEntries.getColumns().size(); i++) {
            TreeTableColumn ttc = listofEntries.getColumns().get(i);
            if (ttc.isVisible()) {
                colCount++;
            }
            
            
        }
        
    
    return colCount;
    
    
    }
    
    
    
    public void setAddressattendancefield(Boolean enabled) {
        this.addressattendancefield.setEditable(enabled);
    }
    
        @FXML
    void deleteButtonPressed(ActionEvent event) {
        
          
        
        

    }
    
    
    public Boolean setTextFields(String eventId,String prompt,String colName,JFXTextField textField){
    
        Boolean isNullable = getifNullable("event_"+eventId, colName);
        
         if (isNullable) {
                        textField.setEditable(false);
                        textField.setPromptText("Disabled");
                    }
                    else{
                        textField.setEditable(true);
                        textField.setPromptText(prompt);
                    }
        
        return isNullable;
    }
    
    @FXML
    void EventsButtonClicked(ActionEvent event) {

       loader.loadMain(ACES.mainmenu,"EventsMenu.fxml");
        Stage stage = (Stage) EventsObject_button.getScene().getWindow();
        stage.hide();
       
    }
    
     
    @FXML
    void saveAttendance(ActionEvent event) {
        //(String eventName,String studNum,String first,String middle,String last,String year,String address,String gender,String age,String contact,LocalTime time,String EventID)
        
     saveData();
        
        
    }
    
    
    public void saveData(){
    
    
       if(!checkFields()){
             sampleInsert("event_"+selected,
                (studentfattendancefield.getText().isEmpty())?null:studentfattendancefield.getText(),
                (firstatendancefield.getText().isEmpty())?null:firstatendancefield.getText(), 
                (middleattendancefield.getText().isEmpty())?null:middleattendancefield.getText(), 
                (lastattendancefield.getText().isEmpty())?null:lastattendancefield.getText(), 
                (yearsectattendancefield.getText().isEmpty())?null:yearsectattendancefield.getText(), 
                (addressattendancefield.getText().isEmpty())?null:addressattendancefield.getText(), 
                (genderattendancefield.getText().isEmpty())?null:genderattendancefield.getText(), 
                (ageattendancefield.getText().isEmpty())?null:ageattendancefield.getText(), 
                (contactattendancefield.getText().isEmpty())?null:contactattendancefield.getText(), 
                LocalTime.now(),(sectionattendancefield.getText().isEmpty())?null:sectionattendancefield.getText());   
             
             studentfattendancefield.setText("");
             firstatendancefield.setText("");
             middleattendancefield.setText("");
             lastattendancefield.setText("");
             yearsectattendancefield.setText("");
             addressattendancefield.setText("");
             genderattendancefield.setText("");
             ageattendancefield.setText("");
             contactattendancefield.setText("");
             sectionattendancefield.setText("");
             
        }
        


        System.out.println(addressattendancefield.getText());
        
        entries.clear();
        initializeTable(selected);
        
        if (addressattendancefield.getText().isEmpty()) {
            System.out.println("Its Null!");
        }
    
    
    
    }
    
    public Boolean checkFields(){
    
        Boolean hasEmpty=false;
        List<String> fields = new ArrayList();
        List<String> enabledfields = new ArrayList();
        List<JFXTextField> textfields = new ArrayList<>();
                textfields.add((studentfattendancefield.getPromptText().equals("Disabled"))?null:studentfattendancefield);
               textfields.add((firstatendancefield.getPromptText().equals("Disabled"))?null:firstatendancefield); 
                textfields.add((middleattendancefield.getPromptText().equals("Disabled"))?null:middleattendancefield) ;
                textfields.add((lastattendancefield.getPromptText().equals("Disabled"))?null:lastattendancefield) ;
                textfields.add((yearsectattendancefield.getPromptText().equals("Disabled"))?null:yearsectattendancefield) ;
                textfields.add((addressattendancefield.getPromptText().equals("Disabled"))?null:addressattendancefield) ;
                textfields.add((genderattendancefield.getPromptText().equals("Disabled"))?null:genderattendancefield) ;
                textfields.add((ageattendancefield.getPromptText().equals("Disabled"))?null:ageattendancefield) ;
                textfields.add((contactattendancefield.getPromptText().equals("Disabled"))?null:contactattendancefield) ;
                textfields.add((sectionattendancefield.getPromptText().equals("Disabled"))?null:sectionattendancefield) ;
        System.out.println(fields);
                
        for (JFXTextField field : textfields) {
            
            if(field!=null && field.getText().equals("")){
                hasEmpty = true;
            }
            
        }
        
       
        
        if (hasEmpty) {
               JFXDialogLayout layout = new JFXDialogLayout();           
           layout.setBody(new Text("Please fill out the necessary fields")); 
           layout.setHeading(new Text("Error!"));
            JFXDialog dialog = new JFXDialog(stackMain, layout, JFXDialog.DialogTransition.CENTER);                
            dialog.setMaxSize(100, 50);          
            PauseTransition delay = new PauseTransition(Duration.millis(2000));
             delay.setOnFinished((event1) -> {               
                 dialog.close();               
             });  
            //layout.setActions(confirm);           
            dialog.show();
            dialog.setVisible(true);
            delay.play();           
        }
        fields.clear();
    return hasEmpty;
    
    }
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        xlName = "19file.xlsx";
        file = new File(filename+xlName);
        table = new ListofEntriesTable();
        workbook = new XSSFWorkbook();
        sheet = workbook.createSheet("Sample");
       // listofEntries = new JFXTreeTableView();
        entries = FXCollections.observableArrayList();
        staticAnchor = anchorPaneAttendance;
        selected = EventsMenuController.selectedEventId;
        openEdit = new TranslateTransition(new Duration(350),anchorPaneAttendance);
        openEdit.setToX(0);
        listofEntries.setVisible(false);
        closeEdit = new TranslateTransition(new Duration(350),anchorPaneAttendance);
       exportButton.setVisible(false);
        name_of_user.setText(LoginController.first + " " + LoginController.middle + " " + LoginController.last);
        position.setText(LoginController.position);
        loader = new CustomLoader();
        anchorPaneAttendance.setVisible(false);
         searchAttendance.setVisible(false);

        if (selected!=null) {
           searchAttendance.setVisible(true);
        anchorPaneAttendance.setVisible(true);
        idplaceholder.setText(selected);
            exportButton.setVisible(true);
        noeventlabel.setVisible(false);
         setColumns();
         initializeTable(selected);
      buildTree();
       
      listofEntries.setVisible(true);
        primarySetTextField();
        
           stackMain.setOnKeyPressed((event) -> {          
            if (event.getCode() == KeyCode.ENTER) {
               saveData();
            }
        });
        
        
        
      
        }
        else openEdit.play();
        
        searchAttendance.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
            
              listofEntries.setPredicate(new Predicate<TreeItem<ListOfEntries>>() {
                         @Override
                         public boolean test(TreeItem<ListOfEntries> t) {
                             Boolean flag = t.getValue().firstname.getValue().toUpperCase().startsWith(newValue.toUpperCase()) ||
                                     t.getValue().lastname.getValue().toUpperCase().startsWith(newValue.toUpperCase());                    
                return flag;
            }
                    });            
            }
        });
        
        
        windowPane.setOnMousePressed((event) -> {
        
            Stage stage = (Stage) ((Pane) event.getSource()).getScene().getWindow();
                this.x = stage.getX() - event.getScreenX();
                this.y = stage.getY() - event.getScreenY();    
        });
        windowPane.setOnMouseDragged((event1) -> {
            Stage stage = (Stage) ((Pane) event1.getSource()).getScene().getWindow();
            stage.setX(event1.getScreenX()+this.x);
            stage.setY(event1.getScreenY()+this.y);
            
        });
        
        
        
     
        
       
        
        
        
        
    }

    public void primarySetColummns(JFXTreeTableColumn column,String eventID,String colName,JFXTextField textfield,String prompt){
    
     if (setTextFields(eventID, prompt ,colName, textfield)) {
                        column.setVisible(false);
        }
        else{
         System.out.println(colName + " is not nullable");
                   column.setVisible(true);
        }
    
    }
    
    public void primarySetTextField(){
    for (int i = 0; i < colNames.length; i++) {
            switch(i){
                case 0:
                    System.out.println(colNames[i]);
                    primarySetColummns(studentnumcolumn, selected, colNames[i], studentfattendancefield, "Student Number");
                break;
                 case 1:                   
                System.out.println(colNames[i]);
                       primarySetColummns(firstcolumn, selected, colNames[i], firstatendancefield, "First Name");
                break;
                 case 2:                   
                    System.out.println(colNames[i]);
                       primarySetColummns(middlecolumn, selected, colNames[i], middleattendancefield, "M.I.");
                break;
                 case 3:
                  System.out.println(colNames[i]);
                     primarySetColummns(lastcolumn, selected, colNames[i], lastattendancefield, "Last Name");
                break;
                 case 4:                 
                   System.out.println(colNames[i]);
                      primarySetColummns(yearsectcolumn, selected, colNames[i], yearsectattendancefield, "Year");
                break;
                 case 6:
                      primarySetColummns(gendercolumn, selected, colNames[i], genderattendancefield, "Gender");
                     System.out.println(colNames[i]);
                     
                break;
                 case 5:  
                     System.out.println(colNames[i]);
                      primarySetColummns(addresscolumn, selected, colNames[i], addressattendancefield, "Address");
                break;
                 case 7:
                  primarySetColummns(agecolumn, selected, colNames[i], ageattendancefield, "Age");
                     
                break;
                 case 8:                                   
                      primarySetColummns(contactcolumn, selected, colNames[i], contactattendancefield, "Contact Number");
                break;
                case 9:                                   
                      primarySetColummns(sectioncolumn, selected, colNames[i], sectionattendancefield, "Section");
                break;
            
                 default:
                     break;
            
            }
            
            
            
        }
        
    
        
    
    }
    
    public void setColumns(){
    
           firstcolumn = new JFXTreeTableColumn<>("First Name");
        firstcolumn.setPrefWidth(150);
       firstcolumn.setCellValueFactory((param) -> {
           if (firstcolumn.validateValue(param)) {
               return param.getValue().getValue().firstname;
           }
           else return firstcolumn.getComputedValue(param);                    
       });
    
     middlecolumn = new JFXTreeTableColumn<>("M.I.");
        middlecolumn.setPrefWidth(50);
       middlecolumn.setCellValueFactory((param) -> {
           if (middlecolumn.validateValue(param)) {
               return param.getValue().getValue().middlename;
           }
           else return middlecolumn.getComputedValue(param);                    
       });
    
        lastcolumn = new JFXTreeTableColumn<>("Last Name");
        lastcolumn.setPrefWidth(100);
       lastcolumn.setCellValueFactory((param) -> {
           if (lastcolumn.validateValue(param)) {
               return param.getValue().getValue().lastname;
           }
           else return lastcolumn.getComputedValue(param);                    
       });
    
       studentnumcolumn = new JFXTreeTableColumn<>("Student Number");
        studentnumcolumn.setPrefWidth(75);
       studentnumcolumn.setCellValueFactory((param) -> {
           if (studentnumcolumn.validateValue(param)) {
               return param.getValue().getValue().studentnum;
           }
           else return studentnumcolumn.getComputedValue(param);                    
       });
       
       yearsectcolumn = new JFXTreeTableColumn<>("Year");
        yearsectcolumn.setPrefWidth(50);
       yearsectcolumn.setCellValueFactory((param) -> {
           if (yearsectcolumn.validateValue(param)) {
               return param.getValue().getValue().yearsect;
           }
           else return yearsectcolumn.getComputedValue(param);                    
       });
       
       agecolumn = new JFXTreeTableColumn<>("Age");
        agecolumn.setPrefWidth(50);
       agecolumn.setCellValueFactory((param) -> {
           if (agecolumn.validateValue(param)) {
               return param.getValue().getValue().age;
           }
           else return agecolumn.getComputedValue(param);                    
       });
       
        gendercolumn = new JFXTreeTableColumn<>("Gender");
        gendercolumn.setPrefWidth(50);
       gendercolumn.setCellValueFactory((param) -> {
           if (gendercolumn.validateValue(param)) {
               return param.getValue().getValue().gender;
           }
           else return gendercolumn.getComputedValue(param);                    
       });
       
       
       contactcolumn = new JFXTreeTableColumn<>("Contact");
        contactcolumn.setPrefWidth(150);
       contactcolumn.setCellValueFactory((param) -> {
           if (contactcolumn.validateValue(param)) {
               return param.getValue().getValue().contact;
           }
           else return contactcolumn.getComputedValue(param);                    
       });
       
       
        timecolumn = new JFXTreeTableColumn<>("Time");
        timecolumn.setPrefWidth(75);
       timecolumn.setCellValueFactory((param) -> {
           if (timecolumn.validateValue(param)) {
               return param.getValue().getValue().timeofarrival;
           }
           else return timecolumn.getComputedValue(param);                    
       });
        
         
        addresscolumn = new JFXTreeTableColumn<>("Address");
        addresscolumn.setPrefWidth(150);
       addresscolumn.setCellValueFactory((param) -> {
           if (addresscolumn.validateValue(param)) {
               return param.getValue().getValue().address;
           }
           else return addresscolumn.getComputedValue(param);                    
       });
       
       
         sectioncolumn = new JFXTreeTableColumn<>("Section");
        sectioncolumn.setPrefWidth(150);
       sectioncolumn.setCellValueFactory((param) -> {
           if (sectioncolumn.validateValue(param)) {
               return param.getValue().getValue().section;
           }
           else return sectioncolumn.getComputedValue(param);                    
       });
       
        
        
    }
    
    
    public Boolean getifNullable(String tableName,String colName){
        Boolean isNullable = false;
        try {
            String query = "SELECT IS_NULLABLE FROM Information_schema.columns WHERE table_name='"+tableName+"' AND COLUMN_NAME='"+colName+"'";
            ConnectDB.rs = ConnectDB.st.executeQuery(query);
            
            while (ConnectDB.rs.next()) {                
                
                isNullable = ConnectDB.rs.getBoolean("IS_NULLABLE");
                
            }
            
            
        } catch (Exception e) {
        
            System.err.println(e);
        }
        
        
        
    return isNullable;
    
    }
    
    
    public void sampleInsert(String eventName,String studNum,String first,String middle,String last,String year,String address,String gender,String age,String contact,LocalTime time,String section){
     try {
             String query = "INSERT INTO "+eventName+"(Student_Number,First_Name,Middle_Initial,Last_Name,Year,Address,Gender,Age,Contact_Number,Time_of_Arrival,Section) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
              PreparedStatement state = ConnectDB.con.prepareStatement(query);
           
              state.setString(1, studNum);
              state.setString(2, first);
              state.setString(3, middle);
              state.setString(4, last);
              state.setString(5, year);
              state.setString(6, address);
              state.setString(7, gender);
              state.setString(8, age);
              state.setString(9, contact);
              state.setTime(10, Time.valueOf(time));
              state.setString(11, section);
              
              System.out.println("Query: " + state.toString());
            state.execute();
              
            JFXDialogLayout layout = new JFXDialogLayout();
            
           layout.setBody(new Text("Added. Welcome!"));
           
            JFXDialog dialog = new JFXDialog(stackMain, layout, JFXDialog.DialogTransition.CENTER);
            
            JFXButton confirm = new JFXButton("Okay");
            
            dialog.setMaxSize(100, 50);
           
            PauseTransition delay = new PauseTransition(Duration.millis(1000));
             delay.setOnFinished((event1) -> {
                
                 dialog.close();
                 
             });
   
            //layout.setActions(confirm);
            
            dialog.show();
            dialog.setVisible(true);
            delay.play();
              
            
          
         } catch (Exception e) {
             System.err.println(e);
         }
 
    }
    
    
   public void buildTree(){
       
    final TreeItem<ListOfEntries> root = new RecursiveTreeItem<ListOfEntries>(entries,RecursiveTreeObject::getChildren);
       // treeTable = new JFXTreeTableView<myEvents>(root);
       listofEntries.setRoot(root);
        System.out.println("Mark 1");
        
       listofEntries.getColumns().setAll(timecolumn,studentnumcolumn,lastcolumn,firstcolumn,middlecolumn,yearsectcolumn,sectioncolumn,gendercolumn,agecolumn,addresscolumn,contactcolumn);
        //System.out.println("Mark 2");
        listofEntries.setShowRoot(false);
        listofEntries.setEditable(true);
        
  
    }
    
   
        public void initializeTable(String eventid){
            //eventid = "1013";
             try {                             
           String query = "SELECT * FROM event_"+eventid;
                //PreparedStatement pr = ConnectDB.con.prepareStatement(query);
                //pr.setString(1, account);
             ConnectDB.rs = ConnectDB.st.executeQuery(query);
             
                while (ConnectDB.rs.next()) {                    
                  entries.add(new ListOfEntries(ConnectDB.rs.getString("First_Name"), ConnectDB.rs.getString("Last_Name"), ConnectDB.rs.getString("Middle_Initial"), ConnectDB.rs.getString("Age"), ConnectDB.rs.getString("Gender"), 
                          ConnectDB.rs.getString("Address"), ConnectDB.rs.getString("Year"), ConnectDB.rs.getString("Contact_Number"), ConnectDB.rs.getString("Student_Number"), ConnectDB.rs.getTime("Time_of_Arrival").toLocalTime(),ConnectDB.rs.getString("Section")));
                    System.out.println("First Name : " + ConnectDB.rs.getString("First_Name"));
                    System.out.println("Gender: " + ConnectDB.rs.getString("Gender"));
                }
       
               
        } catch (SQLException e) {
            System.out.println("Error: " + e);
        }
                 
        } 
    



    
    
}

class ListOfEntries extends RecursiveTreeObject<ListOfEntries>{
  StringProperty firstname,lastname,middlename,age,gender,address,yearsect,contact,studentnum,section;
     SimpleObjectProperty<LocalTime> timeofarrival;

    public ListOfEntries(String firstname, String lastname, String middlename, String age, String gender, String address, String yearsect, String contact, String studentnum, LocalTime timeofarrival,String section) {
        this.firstname = new SimpleStringProperty(firstname);
        this.lastname = new SimpleStringProperty(lastname);
        this.middlename = new SimpleStringProperty(middlename);
        this.age = new SimpleStringProperty(age);
        this.gender = new SimpleStringProperty(gender);
        this.address = new SimpleStringProperty(address);
        this.yearsect = new SimpleStringProperty(yearsect);
        this.contact = new SimpleStringProperty(contact);
        this.studentnum = new SimpleStringProperty(studentnum);
        this.timeofarrival = new SimpleObjectProperty<>(timeofarrival);
        this.section = new SimpleStringProperty(section);
    }


}

class JCustomTextField extends JFXTextField{

    @Override
    public String getText(int start, int end) {
        if (super.getText(start,end) == "") {
            return null;
        }
        else
        return super.getText(start, end); //To change body of generated methods, choose Tools | Templates.
    }




}